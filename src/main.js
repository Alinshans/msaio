// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'

import App from './App'
import router from '@/router/index'
import ElementUI from 'element-ui'
import VeLine from 'v-charts/lib/line.common'
import 'element-ui/lib/theme-chalk/index.css'
import 'xe-utils'
import VXETable from 'vxe-table'
import 'vxe-table/lib/index.css'

Vue.config.productionTip = false
Vue.use(ElementUI)
Vue.component(VeLine.name, VeLine)
Vue.use(VXETable)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
