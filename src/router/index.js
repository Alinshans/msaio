import Vue from 'vue'
import VueRouter from 'vue-router'
Vue.use(VueRouter);

import Home from '@/components/Home'
import Panel from '@/components/Panel'
import Star from '@/components/Star'

import Arcane from '@/components/Calc/Arcane'
import Gollux from '@/components/Calc/Gollux'

import Link from '@/components/Data/Link'
import Exp from '@/components/Data/Exp'
import ExpVisual from '@/components/Data/ExpVisual'
import ExpCalc from '@/components/Data/ExpCalc'
import Boss from '@/components/Data/Boss'
import BossFurniture from '@/components/Data/BossFurniture'
import Haven from '@/components/Data/Haven'
import Pet from '@/components/Data/Pet'
import Cube from '@/components/Data/Cube'

export default new VueRouter({
    routes: [
        { path: "*", redirect: '/home' },
        {
            path: '/home',
            name: 'Home',
            component: Home
        },
        {
            path: '/panel',
            name: 'Panel',
            component: Panel
        },
        {
          path: '/star',
          name: 'Star',
          component: Star
        },
        {
            path: '/arcane',
            name: 'Arcane',
            component: Arcane
        },
        {
            path: '/gollux',
            name: 'Gollux',
            component: Gollux
        },
        {
            path: '/link',
            name: 'Link',
            component: Link
        },
        {
            path: '/exp',
            name: 'Exp',
            component: Exp
        },
        {
            path: '/exp-visual',
            name: 'ExpVisual',
            component: ExpVisual
        },
        {
            path: '/exp-calc',
            name: 'ExpCalc',
            component: ExpCalc
        },
        {
            path: '/boss',
            name: 'Boss',
            component: Boss
        },
        {
            path: '/boss-furniture',
            name: 'BossFurniture',
            component: BossFurniture
        },
        {
            path: '/haven',
            name: 'Haven',
            component: Haven
        },
        {
            path: '/cube',
            name: 'Cube',
            component: Cube
        },
        {
            path: '/pet',
            name: 'Pet',
            component: Pet
        },
    ]
})